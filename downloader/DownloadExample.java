package test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

public class DownloadExample {

    public static void main(String[] args) {
//        Downloader.sequentialDownload();
        Downloader.concurrentDownload2();

    }
    static AtomicInteger count = new AtomicInteger(0);

    // lista plików do pobrania
    static String [] toDownload = {
            "http://home.agh.edu.pl/pszwed/wyklad-c/01-jezyk-c-intro.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/02-jezyk-c-podstawy-skladni.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/03-jezyk-c-instrukcje.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/04-jezyk-c-funkcje.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/05-jezyk-c-deklaracje-typy.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/06-jezyk-c-wskazniki.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/07-jezyk-c-operatory.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/08-jezyk-c-lancuchy-znakow.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/09-jezyk-c-struktura-programow.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/10-jezyk-c-dynamiczna-alokacja-pamieci.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/11-jezyk-c-biblioteka-we-wy.pdf",
            "http://home.agh.edu.pl/~pszwed/wyklad-c/preprocesor-make-funkcje-biblioteczne.pdf",
    };


   static class Downloader implements Runnable{
        private final String url;

        Downloader(String url){
            this.url = url;
        }

        public void run(){
            String fileName = url.substring(url.lastIndexOf("/") + 1);//nazwa pliku z url

            try(InputStream in = new URL(url).openStream(); FileOutputStream out = new FileOutputStream(fileName) ){
                for(;;){
                    if(in.read()<0) break;
                    else out.write(in.read());// czytaj znak z in
                    // jeśli <0 break
                    //zapisz znak do out
                    }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Done:"+fileName);
            count.incrementAndGet();
          //  sem.release();
        }
        static void sequentialDownload(){
            double t1 = System.nanoTime()/1e6;
            for(String url:toDownload){
                new Downloader(url).run();
            }
            double t2 = System.nanoTime()/1e6;
            System.out.printf(Locale.US,"t2-t1=%f\n",t2-t1);
        }
       static void concurrentDownload(){
           double t1 = System.nanoTime()/1e6;
           for(String url:toDownload){
               Thread thread = new Thread(new Downloader(url));
               thread.start();
 //              new Downloader(url).run();
               // uruchom Downloader jako wątek...
           }
           double t2 = System.nanoTime()/1e6;
           System.out.printf(Locale.US,"t2-t1=%f\n",t2-t1);
       }

       static void concurrentDownload2(){
           double t1 = System.nanoTime()/1e6;
           for (String url:toDownload){
               Thread thread = new Thread(new Downloader(url));
               thread.start();
           }
           //while(count!=toDownload.length){
           while(count.get()!=toDownload.length){
               // wait...
               Thread.yield();
           }
           double t2 = System.nanoTime()/1e6;
           System.out.printf(Locale.US,"t2-t1=%f",t2-t1);
        }
       }
    }
