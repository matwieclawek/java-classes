package CV;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class UnorederedList {
List<ListItem> items= new ArrayList<>();

UnorederedList addItem (String item)
{
    items.add(new ListItem(item));
return this;
}

    void writeHTML(PrintStream out){
out.printf("<ul>");
        for (int i=0;i<items.size();i++) {

            items.get(i).writeHTML(out);
        out.printf("\n");
        }
       out.printf("</ul>");
    }

}
