package CV;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class Document {
    String title;
    Photo photo;
    List<Section> sections = new ArrayList<>();

    Document(String title)
    {
        this.title=title;
    }
    Document setTitle(String title){
        this.title = title;
        return this;
    }

    Document setPhoto(String photoUrl,int wymiar1, int wymiar2){
        photo=new Photo(photoUrl,wymiar1, wymiar2);

        return this;
    }

    Section addSection(String sectionTitle){
        Section sec=new Section(sectionTitle);
        sections.add(sec);
        // utwórz sekcję o danym tytule i dodaj do sections
        return sec;
    }
    Document addSection(Section s){
        return this;
    }


    void writeHTML(PrintStream out){
        out.printf("<?xml version=\"1.0\"?>\n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" +
                "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head>\n" +
                "\t<title>%s</title>\n" +
                "\t<meta http-equiv=\"Content-Type\" content=\"application/xhtml+xml;\n" +
                "\t\tcharset=UTF-8\" />"+
                "</head><body><h1>%s</h1>",title,title);

        photo.writeHTML(out);
        for(int i=0;i<sections.size();i++)
        {
            sections.get(i).writeHTML(out);
            out.printf("\n");
        }
        out.printf("</body></html>");
        // zapisz niezbędne znaczniki HTML
        // dodaj tytuł i obrazek
        // dla każdej sekcji wywołaj section.writeHTML(out)
    }

}
