package CV;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class Section {
    String title;
    List<Paragraph> paragraps = new ArrayList<>() ;

    Section (String title)
    {
        this.title=title;
    }
    Section setTitle(String title){
        this.title=title;
        return this;
    }
    Section addParagraph(String paragraphText){
    paragraps.add(new Paragraph().setContent(paragraphText));
        return this;
    }
    Section addParagraph(Paragraph p){
        paragraps.add(p);
        return this;
    }
    void writeHTML(PrintStream out){
        out.printf("<h1> %s</h1>\n",title);
        for(int i=0;i<paragraps.size();i++)
        {
            paragraps.get(i).writeHTML(out);
            out.printf("\n");
        }
    }
}
