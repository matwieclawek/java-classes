package CV;

import java.io.PrintStream;

public class ParagraphWithList extends Paragraph{


    UnorederedList lista;
    ParagraphWithList()
    {
        lista=new UnorederedList();
    }
    ParagraphWithList setContent(String content)
    {
     this.content=content;
     return this;
    }

public ParagraphWithList addListItem(String item)
{
    lista.addItem(item);
    return  this;
}

    @Override
    public void writeHTML(PrintStream out) {
        lista.writeHTML(out);
    }
}
