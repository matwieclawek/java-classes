package CV;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        Document cv = new Document("Zabki - CV");
        cv.setPhoto("zeby.jpg",300,500);
        cv.addSection("Wyksztalcenie")
                .addParagraph("2000-2005 Przedszkole im. Krolewny Sniezki w ...")
                .addParagraph("2006-2012 SP7 im Ronalda Regana w ...")
                .addParagraph("...");
        cv.addSection("Umiejetnosci")
                .addParagraph(
                        new ParagraphWithList().setContent("Umiejetnosci")
                                .addListItem("C")
                                .addListItem("C")
                                .addListItem("C++")
                               .addListItem("Java")

                );


        cv.writeHTML(new PrintStream("cv.html"));
        cv.writeHTML(new PrintStream("C:/Users/Mateusz/Desktop/cv.html","ISO-8859-2"));
    Document cv1= new Document("Kamil Kosiorowski");
    cv1.setPhoto("C:/Users/Mateusz/Desktop/kamil.jpg",350,300);
    cv1.addSection("Informacje podstawowe:")
            .addParagraph("Imie: Kamil")
            .addParagraph("Nazwisko: Kosiorowski")
            .addParagraph("data urodzenia: 25.04.1999")
            .addParagraph("e-mail: kamilk-99@wp.pl");
    cv1.addSection("Wyksztalcenie")
            .addParagraph("Chemia, Politechnika Slaska, 10.2019-")
            .addParagraph("VIII Liceum Ogolnoksztalcace w Katowicach");
    cv1.addSection("Umiejetnosci")
            .addParagraph("Kakutry");
    cv1.writeHTML(new PrintStream("C:/Users/Mateusz/Desktop/cv1.html","ISO-8859-2"));
    }

}
