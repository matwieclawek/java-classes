package CV;

import java.io.PrintStream;

public class Paragraph {
    public String content;

    Paragraph() {
    }

    Paragraph(String content) {
        this.content = content;
    }


    Paragraph setContent(String content) {
        this.content = content;
        return this;
    }

    public void writeHTML(PrintStream out) {
        out.printf("<p>%s</p>", content);
    }


}