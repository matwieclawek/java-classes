package lab8;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminUnitList {
    List<AdminUnit> units = new ArrayList<>();
    private CSVReader c1;
    Map<Long, AdminUnit> idToAdminUnit = new HashMap<>();
    AdminUnit root = new AdminUnit();
    Map<AdminUnit, Long> adminUnitToParentId = new HashMap<>();
    Map<Long, List<AdminUnit>> parentIdToChildren = new HashMap<>();
    Map<Long,List<AdminUnit>> parentid2child = new HashMap<>();


    public void read(String filePath) throws IOException {
        c1 = new CSVReader(filePath,",",true);
        while(c1.next()) {
            AdminUnit adminUnit = new AdminUnit();
            adminUnit.adminLevel = c1.getInt(3);
            adminUnit.name = c1.get(2);
            adminUnit.population = c1.getDouble(4);
            adminUnit.area = c1.getDouble(5);
            adminUnit.density = c1.getDouble(6);
            adminUnit.bbox.xmin = c1.getDouble("x1");
            adminUnit.bbox.xmax = c1.getDouble("x3");
            adminUnit.bbox.ymin = c1.getDouble("y1");
            adminUnit.bbox.ymax = c1.getDouble("y3");

            long parId = c1.getLong("parent");

            idToAdminUnit.put(c1.getLong("id"),adminUnit);
            adminUnitToParentId.put(adminUnit,parId);

            units.add(adminUnit);

            if (parId != -1) {
                if (parentid2child.containsKey(parId)) {
                    parentid2child.get(parId).add(adminUnit);
                } else {
                    List<AdminUnit> children = new ArrayList<>();
                    children.add(adminUnit);
                    parentid2child.put(parId, children);
                }
            }
        }
        for (Map.Entry<AdminUnit, Long> m : adminUnitToParentId.entrySet()){
            AdminUnit key = m.getKey();
            long value = m.getValue();
            for (Map.Entry<Long, AdminUnit> m1 : idToAdminUnit.entrySet()){
                if (m1.getKey() == value && m1.getKey() != -1) {
                    key.parent = m1.getValue();
                    m1.getValue().children.add(key);
                } else if (value == -1){
                    AdminUnit fiction = new AdminUnit();
                    fiction.name = "brak";
                    key.parent = fiction;
                }
            }
        }

        }

    void list(PrintStream out){
        for (AdminUnit a: units)
            out.println(a.toString());
    }
    void getChildren(PrintStream out) {
        for (AdminUnit a : units) {
            a.getChildren(out);
        }
    }

    void list(PrintStream out, int offset, int limit ){
        try {
            for (int i = offset; i < offset + limit; i++) {
                out.println(units.get(i).toString());
            }
        }catch (Exception e){
            e.printStackTrace();
            out.println("List index out of bounds");
        }

    }
    AdminUnitList selectByName(String pattern, boolean regex){
        AdminUnitList ret = new AdminUnitList();
        for (AdminUnit a: units){
            if (regex){
                if (a.name.matches(pattern))
                    ret.units.add(a);
            } else {
                if (a.name.contains(pattern))
                    ret.units.add(a);
            }
        }
        return ret;
    }

    public void fixMissingValues() {
        this.units.forEach(AdminUnit::fixMissingValues);
    }

    AdminUnitList getNeighbors(AdminUnit unit, double maxdistance){
        AdminUnitList ret = new AdminUnitList();

        if (unit.bbox.isEmpty()) return ret;

        for (AdminUnit u: units){
            if (u.adminLevel!=unit.adminLevel)
                continue;
            if(unit.adminLevel == 8){
                if (!u.bbox.isEmpty() && unit.bbox.distanceTo(u.bbox) <= maxdistance)
                    ret.units.add(u);
            } else {
                if (!u.bbox.isEmpty() && unit.bbox.intersects(u.bbox))
                    ret.units.add(u);
            }
        }
        return ret;
    }
}
