package lab8;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Locale;

public class Main {

    public static void main(String[] args) throws IOException {
        String pattern=".*ostom.*";
	AdminUnitList l= new AdminUnitList();
	l.read("C:/Users/Mateusz/Desktop/admin-units.csv");
//l.list(System.out, 100,20);
        l.fixMissingValues();
        l.selectByName(pattern, true).list(System.out);
        l.selectByName(pattern, true).getChildren(System.out);

        l.selectByName(pattern, true).list(new PrintStream("C:/Users/Mateusz/Desktop/plk.txt"));
        l.selectByName(pattern, true).getChildren(new PrintStream("C:/Users/Mateusz/Desktop/plk.txt"));
        double t1 = System.nanoTime()/1e6;
        // wywołanie funkcji

        l.getNeighbors(l.units.get(4), 100);

        double t2 = System.nanoTime()/1e6;
        System.out.printf(Locale.US,"t2-t1=%f\n",t2-t1);

    }
}
