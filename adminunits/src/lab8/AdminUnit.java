package lab8;


import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class AdminUnit {
    String name;
    int adminLevel;
    double population;
    double area;
    double density;
    AdminUnit parent;
    BoundingBox bbox = new BoundingBox();
    List<AdminUnit> children = new ArrayList<>();

    public void getChildren(PrintStream out){
    for(AdminUnit a: children)
    {
        out.println(a.toString());

    }

    }


    public String toString() {
        if (!name.equals("brak")) {
            String ret=
            "AdminUnit{"
                    + "name='"
                    + name
                    + '\''
                    + ", adminLevel="
                    + adminLevel
                    + ", population="
                    + population
                    + ", area="
                    + area
                    + ", density="
                    + density;
            if(!parent.name.equals("brak")) {
                ret +=
                        ", parent="
                                + parent;
            }
            ret+=
                    ", bbox="
                     + bbox
                     + '}';

        return ret;
        }
        else return " ";
    }
    public void fixMissingValues() {
        if (this.parent != null && (this.parent.area == -1 || this.parent.density== -1))
            this.parent.fixMissingValues();
        if (this.parent != null && this.area == -1)
            this.area = this.parent.area;
        if (this.parent != null && this.density == -1)
            this.density = this.parent.density;
        if (this.population == -1)
            this.population = this.area * this.density;
    }
}