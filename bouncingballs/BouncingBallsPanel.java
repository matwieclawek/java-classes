package test;


import javax.swing.*;
//import java.awt.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BouncingBallsPanel extends JPanel {

    static class Ball {
        int x;
        int y;
        double vx;
        double vy;
        Color color;

        public Ball(int _x, int _y, double _vx, double _vy, Color c) {
            this.y = _y;
            this.x = _x;
            this.vx = _vx;
            this.vy = _vy;
            this.color = c;
        }

        public void changeSpeedVecotr() {
            if (x + vx < 0 || x + 20 + vx > 700) {
                vx *= -1;
            }
            if (y + vy < 0 || y + 20 + vy > 630) {
                vy *= -1;
            }
            this.x += this.vx;
            this.y += this.vy;

        }
    }

        void collision(Ball b) {
            for (int i = 0; i < balls.size(); i++) {
                Ball b2 = balls.get(i);
                if (b2 != b) {
                    int dx = Math.abs(b2.x - b.x);
                    int dy = Math.abs(b2.y - b.y);
                    int distSqr = (dx * dx) + (dy * dy);
                    if (distSqr < 160) {
                        b.vx *= -1;
                        b.vy *= -1;
                        b2.vx *= -1;
                        b2.vy *= -1;
                    }
                }
            }
        }




    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        for (Ball b : balls) {
            g2.setColor(b.color);
            g2.fillOval(b.x, b.y, 20,20);
        }
    }

    List<Ball> balls = new ArrayList<>();

    class AnimationThread extends Thread{
        boolean sus=true;
        public void pause()
        {
            sus=true;
        }
        public synchronized  void stoppause()
        {
            sus=false;
        notify();
        }

        public void run() {
            while (true) {
                for (Ball ball : balls) {
                    ball.changeSpeedVecotr();
                   collision(ball);
                }
                repaint();

                synchronized (this) {
                    try {
                        if (sus) {
                            System.out.println("Suspending");
                            wait();
                        }
                    } catch (InterruptedException e) {
                    }
                }

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private AnimationThread anim=new AnimationThread();
int height, width;

    BouncingBallsPanel(int height, int width)
    {
        this.width=width;
        this.height=height;
        anim.start();
        setBorder(BorderFactory.createStrokeBorder(new BasicStroke(3.0f)));
    }




    void onStart(){
        System.out.println("Start or resume animation thread");
      // anim.start();
         anim.stoppause();
    }

    void onStop(){
        System.out.println("Suspend animation thread");
        anim.pause();
    }

    void onPlus(){
        Random rand=new Random();
        Color c= new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
        balls.add(new Ball(50 + rand.nextInt(width - 100), 50 + rand.nextInt(height - 100), rand.nextInt(20) , rand.nextInt(20) , c));
        System.out.println("Add a ball");
    }

    void onMinus() {
        if (balls.isEmpty())
            System.out.println("Is empty");
        else {
            balls.remove(balls.size() - 1);
            System.out.println("Remove a ball");
        }
    }
}