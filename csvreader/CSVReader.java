package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CSVReader {
    private BufferedReader reader;
    private String delimiter;
    private boolean hasHeader;
    public String[]current;
    private int currentlength;

    private static final String DEFAULT_ENCODING = "UTF-8";


    List<String> columnLabels = new ArrayList<>();
    Map<String,Integer> columnLabelsToInt = new HashMap<>();

    public CSVReader(Reader reader, String delimiter, boolean hasHeader) throws IOException {
        this.columnLabelsToInt = new HashMap<>();
        this.reader = new BufferedReader(reader);
        this.delimiter = delimiter;
        this.hasHeader = hasHeader;

        if (hasHeader) {
            parseHeader();
        }
    }

    List<String> getColumnLabels()
    {
        return columnLabels;
    }

    CSVReader(String filename,String delimiter) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(filename));
        this.delimiter=delimiter;
    }

    void parseHeader() throws IOException {
        String line = reader.readLine();
        if (line == null) {
            return;
        }
        String[] header = line.split(delimiter);
        for (int i = 0; i < header.length; i++) {
            columnLabels.add(header[i]);
            columnLabelsToInt.put(header[i],i);
        }
    }


    public CSVReader(String filename,String delimiter,boolean hasHeader) throws IOException {
        reader = new BufferedReader(new FileReader(filename));
        this.delimiter = delimiter;
        this.hasHeader = hasHeader;
        if(hasHeader)parseHeader();
    }


    public boolean next() throws IOException {
        String line = reader.readLine();
        if(line==null)return false;
        //String line = reader.readLine();
        this.current = line.split(delimiter);
        currentlength=current.length;
        // czyta następny wiersz, dzieli na elementy i przypisuje do current
        //
        return true;
    }

    public String get(int columnIndex)
    {
        return current[columnIndex];
    }
    public int getRecordLength(){
        return currentlength;
    }

    public int getInt(int columnIndex){
        return Integer.parseInt(current[columnIndex]);

    }
    public int getInt(String columnLabel){
     //   int w=columnLabelsToInt.get(columnLabel);
        if(!columnLabelsToInt.containsKey(columnLabel))
        {
            return 0;
        }
        return Integer.parseInt(current[columnLabelsToInt.get(columnLabel)]);

    }

    public double getDouble(int columnIndex){
        double c=Double.parseDouble(current[columnIndex]);
        return c;

    }

   public double getDouble(String columnLabel){
       if(!columnLabelsToInt.containsKey(columnLabel))
       {
           return 0;
       }

        int w=columnLabelsToInt.get(columnLabel);
        double z=Double.parseDouble(current[w]);
        return Double.parseDouble(current[columnLabelsToInt.get(columnLabel)]);

    }

    public String get(String columnLabel)
    {
        if(!columnLabelsToInt.containsKey(columnLabel))
        {
        return "Błąd";
        }
        return current[columnLabelsToInt.get(columnLabel)];
    }
    public boolean isMissing(int index) {
        return index < 0 || index >= current.length || current[index].isEmpty();
    }
    public boolean isMissing(String column) {
        return isMissing(columnLabelsToInt.getOrDefault(column, -1));
    }


}

